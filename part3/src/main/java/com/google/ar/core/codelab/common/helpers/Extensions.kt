package com.google.ar.core.codelab.common.helpers


import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.*
import java.util.zip.ZipFile

object Extensions {
    @RequiresApi(Build.VERSION_CODES.O)
    fun saveByteArrayToFile(fileName: String, context: Context, byteArray: ByteArray) {
        val fileDirectory = context.filesDir
        var counter = 0
        var inputFile = File(fileDirectory.path + File.separator + "output.txt")
        var outputFile = File(fileDirectory.path + File.separator + "output.zip")

        val fileOutputStream = FileOutputStream(outputFile)
        val fileInputStream = FileInputStream(inputFile)
        val bytes = fileInputStream.readBytes()
        val decoded: ByteArray = Base64.getDecoder().decode(bytes)
        fileOutputStream.write(decoded)
        fileOutputStream.close()
        fileInputStream.close()
    }

    fun dearchive(context: Context, file: File) {
        val zipFile = ZipFile(file)
        zipFile.use { zip ->
            zip.entries().asSequence().forEach { entry ->
                zip.getInputStream(entry).use { input ->
                    val filePath = context.filesDir.path + File.separator + entry.name
                    if (!entry.isDirectory) {
                        // if the entry is a file, extracts i
                        val file = File(filePath)
                        file.writeBytes(input.readBytes())
                    } else {
                        // if the entry is a directory, make the directory
                        val dir = File(filePath)
                        dir.mkdir()
                    }
                }
            }
        }
    }
}


