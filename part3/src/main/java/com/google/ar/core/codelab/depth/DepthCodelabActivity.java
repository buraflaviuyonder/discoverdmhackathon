/*
 * Copyright 2020 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.core.codelab.depth;

import android.app.Activity;
import android.media.Image;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.ar.core.Anchor;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Camera;
import com.google.ar.core.CameraIntrinsics;
import com.google.ar.core.Config;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.codelab.common.helpers.CameraPermissionHelper;
import com.google.ar.core.codelab.common.helpers.DisplayRotationHelper;
import com.google.ar.core.codelab.common.helpers.FullScreenHelper;
import com.google.ar.core.codelab.common.helpers.SnackbarHelper;
import com.google.ar.core.codelab.common.helpers.TapHelper;
import com.google.ar.core.codelab.common.helpers.TrackingStateHelper;
import com.google.ar.core.codelab.common.rendering.BackgroundRenderer;
import com.google.ar.core.codelab.common.rendering.ObjectRenderer;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.NotYetAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using the
 * ARCore API. The application will allow the user to tap to place a 3d model of the Android robot.
 */
public class DepthCodelabActivity extends Activity implements GLSurfaceView.Renderer {
    private static final String TAG = DepthCodelabActivity.class.getSimpleName();

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private GLSurfaceView surfaceView;

    private boolean isMeasureDone = true;
    private boolean installRequested;
    private boolean isDepthSupported;

    private Session session;
    private final SnackbarHelper messageSnackbarHelper = new SnackbarHelper();
    private DisplayRotationHelper displayRotationHelper;
    private final TrackingStateHelper trackingStateHelper = new TrackingStateHelper(this);
    private TapHelper tapHelper;

    private final DepthTextureHandler depthTexture = new DepthTextureHandler();
    private BackgroundRenderer backgroundRenderer = new BackgroundRenderer(1);
    private BackgroundRenderer backgroundRenderer2 = new BackgroundRenderer(2);
    private final ObjectRenderer virtualObject = new ObjectRenderer();
    private int tip = 1;
    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private final float[] anchorMatrix = new float[16];
    private static final String SEARCHING_PLANE_MESSAGE = "Please move around slowly...";
    private static final String PLANES_FOUND_MESSAGE = "Hello Salty Dwarfs";
    private static final String DEPTH_NOT_AVAILABLE_MESSAGE = "[Depth not supported on this device]";
    // Anchors created from taps used for object placing with a given color.
    private static final float[] OBJECT_COLOR = new float[]{139.0f, 195.0f, 74.0f, 255.0f};
    private final ArrayList<Anchor> anchors = new ArrayList<>();

    private int noCrtFrames = 1;
    private List<Frame> frames = new ArrayList<>();
    private List<Float> pointsFrames = new ArrayList<>();
    private boolean showDepthMap = false;
    private Frame frame;
    private Camera camera;
    private Boolean isFinished = true;
    private Boolean isClickedButton = false;
    private ImageView crossImageView;
    private Boolean isFABOpen = false;

    private void showFABMenu() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_selected);
        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        FloatingActionButton fab4 = (FloatingActionButton) findViewById(R.id.fab4);
        isFABOpen = true;
        fab1.animate().translationY(+getResources().getDimension(R.dimen.standard_55));
        fab2.animate().translationY(+getResources().getDimension(R.dimen.standard_105));
        fab3.animate().translationY(+getResources().getDimension(R.dimen.standard_155));
        fab4.animate().translationY(+getResources().getDimension(R.dimen.standard_210));
    }

    private void closeFABMenu() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_selected);
        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        FloatingActionButton fab4 = (FloatingActionButton) findViewById(R.id.fab4);
        isFABOpen = false;
        fab1.animate().translationY(0);
        fab2.animate().translationY(0);
        fab3.animate().translationY(0);
        fab4.animate().translationY(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1500);

        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeOut);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                findViewById(R.id.splash1).setVisibility(View.GONE);
                findViewById(R.id.splash2).setVisibility(View.GONE);
                findViewById(R.id.progress_circular).setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        findViewById(R.id.splash1).startAnimation(animation);


        surfaceView = findViewById(R.id.surfaceview);
        crossImageView = findViewById(R.id.cross);
        displayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

        // Set up tap listener.
        tapHelper = new TapHelper(/*context=*/ this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_selected);
        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        FloatingActionButton fab4 = (FloatingActionButton) findViewById(R.id.fab4);
        if (tip == 2) {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_wave));
        } else {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_heatmap));
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }
            }
        });

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.cross).setVisibility(View.GONE);
                findViewById(R.id.length_value).setVisibility(View.GONE);
                findViewById(R.id.capture_frame_image).setVisibility(View.GONE);
                findViewById(R.id.succes_message).setVisibility(View.GONE);
                findViewById(R.id.toggle_depth_button).setVisibility(View.VISIBLE);
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_wave));
                tip = 2;
                closeFABMenu();
                onPause();
                onResume();
                findViewById(R.id.layout_custom).invalidate();
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.cross).setVisibility(View.GONE);
                findViewById(R.id.length_value).setVisibility(View.GONE);
                findViewById(R.id.capture_frame_image).setVisibility(View.GONE);
                findViewById(R.id.succes_message).setVisibility(View.GONE);
                findViewById(R.id.toggle_depth_button).setVisibility(View.VISIBLE);
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_heatmap));
                tip = 1;
                closeFABMenu();
                onPause();
                onResume();
                findViewById(R.id.layout_custom).invalidate();
            }
        });

        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.cross).setVisibility(View.VISIBLE);
                findViewById(R.id.length_value).setVisibility(View.VISIBLE);
                findViewById(R.id.capture_frame_image).setVisibility(View.GONE);
                findViewById(R.id.succes_message).setVisibility(View.GONE);
                findViewById(R.id.toggle_depth_button).setVisibility(View.GONE);
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_measure));
                closeFABMenu();
            }
        });

        fab4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.cross).setVisibility(View.GONE);
                findViewById(R.id.length_value).setVisibility(View.GONE);
                findViewById(R.id.capture_frame_image).setVisibility(View.VISIBLE);
                findViewById(R.id.toggle_depth_button).setVisibility(View.GONE);
                fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_capture));
                closeFABMenu();
            }
        });


        // Set up renderer.
        surfaceView.setPreserveEGLContextOnPause(true);
        surfaceView.setEGLContextClientVersion(2);
        surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        surfaceView.setRenderer(this);
        surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
        surfaceView.setWillNotDraw(false);

        installRequested = false;
        findViewById(R.id.capture_frame_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(50); //You can manage the blinking time with this parameter
                anim.setStartOffset(20);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(4);
                findViewById(R.id.succes_message).setVisibility(View.VISIBLE);
                findViewById(R.id.capture_frame_image).setVisibility(View.GONE);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        findViewById(R.id.succes_message).setVisibility(View.GONE);
                        findViewById(R.id.capture_frame_image).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                findViewById(R.id.succes_message).startAnimation(anim);
                isClickedButton = true;
            }
        });

        final FloatingActionButton toggleDepthButton = (FloatingActionButton) findViewById(R.id.toggle_depth_button);
        toggleDepthButton.setOnClickListener(
                view -> {
                    if (isDepthSupported) {
                        showDepthMap = !showDepthMap;
                        toggleDepthButton.setImageDrawable(getResources().getDrawable(showDepthMap ? R.drawable.baseline_visibility_black_24dp : R.drawable.baseline_visibility_off_black_24dp));
                    } else {
                        showDepthMap = false;
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (session == null) {
            Exception exception = null;
            String message = null;
            try {
                switch (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
                    case INSTALL_REQUESTED:
                        installRequested = true;
                        return;
                    case INSTALLED:
                        break;
                }

                // ARCore requires camera permissions to operate. If we did not yet obtain runtime
                // permission on Android M and above, now is a good time to ask the user for it.
                if (!CameraPermissionHelper.hasCameraPermission(this)) {
                    CameraPermissionHelper.requestCameraPermission(this);
                    return;
                }

                // Creates the ARCore session.
                session = new Session(/* context= */ this);
                Config config = session.getConfig();
                isDepthSupported = session.isDepthModeSupported(Config.DepthMode.AUTOMATIC);
                if (isDepthSupported) {
                    config.setDepthMode(Config.DepthMode.AUTOMATIC);
                } else {
                    config.setDepthMode(Config.DepthMode.DISABLED);
                }
                session.configure(config);

            } catch (UnavailableArcoreNotInstalledException
                    | UnavailableUserDeclinedInstallationException e) {
                message = "Please install ARCore";
                exception = e;
            } catch (UnavailableApkTooOldException e) {
                message = "Please update ARCore";
                exception = e;
            } catch (UnavailableSdkTooOldException e) {
                message = "Please update this app";
                exception = e;
            } catch (UnavailableDeviceNotCompatibleException e) {
                message = "This device does not support AR";
                exception = e;
            } catch (Exception e) {
                message = "Failed to create AR session";
                exception = e;
            }

            if (message != null) {
                messageSnackbarHelper.showError(this, message);
                Log.e(TAG, "Exception creating session", exception);
                return;
            }
        }

        // Note that order matters - see the note in onPause(), the reverse applies here.
        try {
            session.resume();
        } catch (CameraNotAvailableException e) {
            messageSnackbarHelper.showError(this, "Camera not available. Try restarting the app.");
            session = null;
            return;
        }

        surfaceView.onResume();
        displayRotationHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (session != null) {
            // Note that the order matters - GLSurfaceView is paused first so that it does not try
            // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
            // still call session.update() and get a SessionPausedException.
            displayRotationHelper.onPause();
            surfaceView.onPause();
            session.pause();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            Toast.makeText(this, "Camera permission is needed to run this application",
                    Toast.LENGTH_LONG).show();
            if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                CameraPermissionHelper.launchPermissionSettings(this);
            }
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        FullScreenHelper.setFullScreenOnWindowFocusChanged(this, hasFocus);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
        try {
            lengthValue = findViewById(R.id.length_value);
            // The depth texture is used for object occlusion and rendering.
            depthTexture.createOnGlThread();

            // Create the texture and pass it to ARCore session to be filled during update().
            backgroundRenderer.createOnGlThread(/*context=*/ this);
            backgroundRenderer.createDepthShaders(/*context=*/ this, depthTexture.getDepthTexture());

            backgroundRenderer2.createOnGlThread(/*context=*/ this);
            backgroundRenderer2.createDepthShaders(/*context=*/ this, depthTexture.getDepthTexture());

            virtualObject.createOnGlThread(/*context=*/ this, "models/andy.obj", "models/andy.png");
            virtualObject.setMaterialProperties(0.0f, 2.0f, 0.5f, 6.0f);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read an asset file", e);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        displayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    private TextView lengthValue;

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (session == null) {
            return;
        }
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        displayRotationHelper.updateSessionIfNeeded(session);

        try {
            if (tip == 1) {
                session.setCameraTextureName(backgroundRenderer.getTextureId());
            } else {
                session.setCameraTextureName(backgroundRenderer2.getTextureId());
            }

            // Obtain the current frame from ARSession. When the configuration is set to
            // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
            // camera framerate.
            Frame frame = session.update();
            Camera camera = frame.getCamera();
            this.frame = frame;

            this.camera = camera;
            // Retrieves the latest depth image for this frame.
            if (isDepthSupported) {
                depthTexture.update(frame);
            }

            // Handle one tap per frame.
            //handleTap(frame, camera);
            tapHelper.poll();


            if (tip == 1) {
                // If frame is ready, render camera preview image to the GL surface.
                backgroundRenderer.draw(frame);
                if (showDepthMap) {
                    backgroundRenderer.drawDepth(frame);
                }
            } else {
                backgroundRenderer2.draw(frame);
                if (showDepthMap) {
                    backgroundRenderer2.drawDepth(frame);
                }
            }

            // Keep the screen unlocked while tracking, but allow it to lock when tracking stops.
            trackingStateHelper.updateKeepScreenOnFlag(camera.getTrackingState());

//            // If not tracking, don't draw 3D objects, show tracking failure reason instead.
//            if (camera.getTrackingState() == TrackingState.PAUSED) {
//                messageSnackbarHelper.showMessage(
//                        this, TrackingStateHelper.getTrackingFailureReasonString(camera));
//                return;
//            }

            // Get projection matrix.
            float[] projmtx = new float[16];
            camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);

            // Get camera matrix and draw.
            float[] viewmtx = new float[16];
            camera.getViewMatrix(viewmtx, 0);

            // Compute lighting from average intensity of the image.
            // The first three components are color scaling factors.
            // The last one is the average pixel intensity in gamma space.
            final float[] colorCorrectionRgba = new float[4];
            frame.getLightEstimate().getColorCorrection(colorCorrectionRgba, 0);

            // No tracking error at this point. Inform user of what to do based on if planes are found.
//            String messageToShow = "";
//            if (hasTrackingPlane()) {
//                messageToShow = PLANES_FOUND_MESSAGE;
//            } else {
//                messageToShow = SEARCHING_PLANE_MESSAGE;
//            }
//            if (!isDepthSupported) {
//                messageToShow += "\n" + DEPTH_NOT_AVAILABLE_MESSAGE;
//            }
//            messageSnackbarHelper.showMessage(this, messageToShow);

            // Visualize anchors created by touch.
            float scaleFactor = 1.0f;
            for (Anchor anchor : anchors) {
                if (anchor.getTrackingState() != TrackingState.TRACKING) {
                    continue;
                }
                // Get the current pose of an Anchor in world space. The Anchor pose is updated
                // during calls to session.update() as ARCore refines its estimate of the world.
                anchor.getPose().toMatrix(anchorMatrix, 0);

                // Update and draw the model and its shadow.
                virtualObject.updateModelMatrix(anchorMatrix, scaleFactor);
                virtualObject.draw(viewmtx, projmtx, colorCorrectionRgba, OBJECT_COLOR);
            }

            measureDistance();
            create(frame, session.createAnchor(camera.getPose()));
        } catch (Throwable t) {
            // Avoid crashing the application due to unhandled exceptions.
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }

    private void sendDataToPythonAPI(List<Float> points) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                String newLine = System.getProperty("line.separator");
                StringBuilder exportData = new StringBuilder();
                for (int i = 0; i < points.size() - 6; i = i + 3) {
                    float x = points.get(i);
                    float y = points.get(i + 1);
                    float z = points.get(i + 2);
                    exportData = exportData
                            .append(x)
                            .append(" ")
                            .append(y)
                            .append(" ")
                            .append(z)
                            .append(newLine);
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    String encodedString = Base64.getEncoder().encodeToString(exportData.toString().getBytes());
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("points", encodedString);
                        try {
                            URL url = new URL("http://192.168.0.92:5000/process_android");
                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setRequestMethod("POST"); // here you are telling that it is a POST request, which can be changed into "PUT", "GET", "DELETE" etc.
                            httpURLConnection.setRequestProperty("Content-Type", "application/json"); // here you are setting the `Content-Type` for the data you are sending which is `application/json`
                            httpURLConnection.connect();

                            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                            wr.writeBytes(jsonObject.toString());
                            wr.flush();
                            wr.close();

                            // get response
                            InputStream responseStream = new BufferedInputStream(httpURLConnection.getInputStream());
                            BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                            String line = "";
                            StringBuilder stringBuilder = new StringBuilder();
                            while ((line = responseStreamReader.readLine()) != null) {
                                stringBuilder.append(line);
                            }
                            responseStreamReader.close();

                            String response = stringBuilder.toString();
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.get("result").equals("Ok")) {
                                runOnUiThread(() -> {
                                    Toast.makeText(getApplicationContext(), R.string.message_succesfull, Toast.LENGTH_LONG).show();
                                });
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, 50);
    }

    private double calculateDistance(Float x, Float y, Float z) {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    private double calculateDistance(Pose cameraPose, float x, float y, float z) {
        return calculateDistance(
                x - cameraPose.tx(),
                y - cameraPose.ty(),
                z - cameraPose.tz()
        );
    }

    // Handle only one tap per frame, as taps are usually low frequency compared to frame rate.
    private void measureDistance() {
        if (camera.getTrackingState() == TrackingState.TRACKING) {
            for (HitResult hit : frame.hitTest(crossImageView.getX(), crossImageView.getY())) {

                if (isMeasureDone) {
                    isMeasureDone = false;
                    lengthValue.postDelayed(() -> {
                        lengthValue.setText(String.format("%.2f", hit.getDistance()) + " meters");
                        isMeasureDone = true;
                    }, 300);
                }
            }
        }
    }


    // Checks if we detected at least one plane.
    private boolean hasTrackingPlane() {
        for (Plane plane : session.getAllTrackables(Plane.class)) {
            if (plane.getTrackingState() == TrackingState.TRACKING) {
                return true;
            }
        }
        return false;
    }

    // Calculate the normal distance to plane from cameraPose, the given planePose should have y axis
    // parallel to plane's normal, for example plane's center pose or hit test pose.
    private static float calculateDistanceToPlane(Pose planePose, Pose cameraPose) {
        float[] normal = new float[3];
        float cameraX = cameraPose.tx();
        float cameraY = cameraPose.ty();
        float cameraZ = cameraPose.tz();
        // Get transformed Y axis of plane's coordinate system.
        planePose.getTransformedAxis(1, 1.0f, normal, 0);
        // Compute dot product of plane's normal with vector from camera to plane center.
        return (cameraX - planePose.tx()) * normal[0]
                + (cameraY - planePose.ty()) * normal[1]
                + (cameraZ - planePose.tz()) * normal[2];
    }

    public static final int FLOATS_PER_POINT = 4; // X,Y,Z,confidence.

    public FloatBuffer create(Frame frame, Anchor cameraPoseAnchor) {
        try {
            Image depthImage = frame.acquireRawDepthImage();
            Image confidenceImage = frame.acquireRawDepthConfidenceImage();

            // To transform 2D depth pixels into 3D points we retrieve the intrinsic camera parameters
            // corresponding to the depth image. See more information about the depth values at
            // https://developers.google.com/ar/develop/java/depth/overview#understand-depth-values.
            final CameraIntrinsics intrinsics = frame.getCamera().getTextureIntrinsics();
            float[] modelMatrix = new float[16];
            cameraPoseAnchor.getPose().toMatrix(modelMatrix, 0);
            final FloatBuffer points = convertRawDepthImagesTo3dPointBuffer(
                    depthImage, confidenceImage, intrinsics, modelMatrix);

            depthImage.close();
            confidenceImage.close();

            return points;
        } catch (NotYetAvailableException e) {
            // This normally means that depth data is not available yet. This is normal so we will not
            // spam the logcat with this.
        }
        return null;
    }

    /**
     * Applies camera intrinsics to convert depth image into a 3D pointcloud.
     */
    private FloatBuffer convertRawDepthImagesTo3dPointBuffer(
            Image depth, Image confidence, CameraIntrinsics cameraTextureIntrinsics, float[] modelMatrix) {
        // Java uses big endian so we have to change the endianess to ensure we extract
        // depth data in the correct byte order.
        final Image.Plane depthImagePlane = depth.getPlanes()[0];
        ByteBuffer depthByteBufferOriginal = depthImagePlane.getBuffer();
        ByteBuffer depthByteBuffer = ByteBuffer.allocate(depthByteBufferOriginal.capacity());
        depthByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        while (depthByteBufferOriginal.hasRemaining()) {
            depthByteBuffer.put(depthByteBufferOriginal.get());
        }
        depthByteBuffer.rewind();
        ShortBuffer depthBuffer = depthByteBuffer.asShortBuffer();

        final Image.Plane confidenceImagePlane = confidence.getPlanes()[0];
        ByteBuffer confidenceBufferOriginal = confidenceImagePlane.getBuffer();
        ByteBuffer confidenceBuffer = ByteBuffer.allocate(confidenceBufferOriginal.capacity());
        confidenceBuffer.order(ByteOrder.LITTLE_ENDIAN);
        while (confidenceBufferOriginal.hasRemaining()) {
            confidenceBuffer.put(confidenceBufferOriginal.get());
        }
        confidenceBuffer.rewind();

        // To transform 2D depth pixels into 3D points we retrieve the intrinsic camera parameters
        // corresponding to the depth image. See more information about the depth values at
        // https://developers.google.com/ar/develop/java/depth/overview#understand-depth-values.
        final int[] intrinsicsDimensions = cameraTextureIntrinsics.getImageDimensions();
        final int depthWidth = depth.getWidth();
        final int depthHeight = depth.getHeight();
        final float fx =
                cameraTextureIntrinsics.getFocalLength()[0] * depthWidth / intrinsicsDimensions[0];
        final float fy =
                cameraTextureIntrinsics.getFocalLength()[1] * depthHeight / intrinsicsDimensions[1];
        final float cx =
                cameraTextureIntrinsics.getPrincipalPoint()[0] * depthWidth / intrinsicsDimensions[0];
        final float cy =
                cameraTextureIntrinsics.getPrincipalPoint()[1] * depthHeight / intrinsicsDimensions[1];

        // Allocate the destination point buffer. If the number of depth pixels is larger than
        // `maxNumberOfPointsToRender` we uniformly subsample. The raw depth image may have
        // different resolutions on different devices.
        final float maxNumberOfPointsToRender = 20000;
        int step = (int) Math.ceil(Math.sqrt(depthWidth * depthHeight / maxNumberOfPointsToRender));

        FloatBuffer points = FloatBuffer.allocate(depthWidth / step * depthHeight / step * FLOATS_PER_POINT);
        float[] pointCamera = new float[4];
        float[] pointWorld = new float[4];
        pointsFrames.clear();
        for (int y = 0; y < depthHeight; y += step) {
            for (int x = 0; x < depthWidth; x += step) {
                // Depth images are tightly packed, so it's OK to not use row and pixel strides.
                int depthMillimeters = depthBuffer.get(y * depthWidth + x); // Depth image pixels are in mm.
                if (depthMillimeters == 0) {
                    // Pixels with value zero are invalid, meaning depth estimates are missing from
                    // this location.
                    continue;
                }
                final float depthMeters = depthMillimeters / 1000.0f; // Depth image pixels are in mm.

                // Retrieves the confidence value for this pixel.
                final byte confidencePixelValue =
                        confidenceBuffer.get(
                                y * confidenceImagePlane.getRowStride()
                                        + x * confidenceImagePlane.getPixelStride());
                final float confidenceNormalized = ((float) (confidencePixelValue & 0xff)) / 255.0f;

                // Unprojects the depth into a 3D point in camera coordinates.
                pointCamera[0] = depthMeters * (x - cx) / fx;
                pointCamera[1] = depthMeters * (cy - y) / fy;
                pointCamera[2] = -depthMeters;
                pointCamera[3] = 1;

                // Applies model matrix to transform point into world coordinates.
                Matrix.multiplyMV(pointWorld, 0, modelMatrix, 0, pointCamera, 0);
                points.put(pointWorld[0]); // X.
                points.put(pointWorld[1]); // Y.
                points.put(pointWorld[2]); // Z.
                pointsFrames.add(pointWorld[0]); // X.
                pointsFrames.add(pointWorld[1]); // Y.
                pointsFrames.add(pointWorld[2]); // Z.
                points.put(confidenceNormalized);
            }
        }
        if (isClickedButton) {
            sendDataToPythonAPI(new ArrayList<>(pointsFrames));
            isClickedButton = false;
        }

        points.rewind();
        return points;
    }

}
